# Debian 30 years logo



## Roteiro em português

1. Tarefa prioritária: desenvolver uma marca de 30 anos para que possa ser usada em conteúdos e materiais ao redor do mundo, desde template para fotos e vídeos, até produção de camisetas e outros materiais. 

2. Disponibilizar a marca via Salsa com manual de uso e templates para postagens, stickers, camisetas, e o que for necessário.

3. Criação de videoloop para materiais animados com a marca

Chamaremos uma campanha para que as pessoas postem e usem a marca em comemorações oficiais e também individuais, e que postem em suas redes sempre marcando as respectivas redes e links do projeto Debian. Também serão incentivados depoimentos por escrito usando hastags nas redes que permitirem.

Será feita uma página na wiki para listar e reunir os registros das comemorações ao redor do mundo, em imagem, vídeo etc. 
